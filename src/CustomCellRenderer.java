import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;

public class CustomCellRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table,
			Object value,
			boolean isSelected,
			boolean hasFocus,
			int row,
			int column) {
		Component c = 
				super.getTableCellRendererComponent(table, value,
						isSelected, hasFocus,
						row, column);

		// Only for specific cell
		if ("Invalid".equals(value)) {
			c.setForeground(Color.RED);
		} else {
			c.setForeground(Color.BLACK);
		}
		if (0 == column) {
			c.setFont(c.getFont().deriveFont(Font.BOLD));
		}

		return c;
	}
}