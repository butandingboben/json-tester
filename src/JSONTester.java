import java.io.File;

import org.json.JSONObject;

import com.rococo.loader.util.JSONExcelUtil;

/**
 * @author Joven 'Bob' Montero
 *
 */
public class JSONTester {

	// Constants
	/**
	 * The directory of the test file.
	 * Note: must have trailing slashes.
	 */
	private static final String FILE_DIRECTORY = "C:\\Users\\butandingboben\\Desktop\\excel\\";
	/**
	 * The name of the source file.
	 */
	private static final String FILE_NAME = "TestFile.xls";
	private static final int JSON_DATA = 0;
	private static final int JSON_EXPECTED = 1;
	private static final int DATASTORE_EXPECTED = 3;

	private static JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
	private static File fileSource = new File(FILE_DIRECTORY + FILE_NAME);

	public static void main(String[] args) {
		if (fileSource.exists()) {

			// Function call for loading the file and retrieving all available
			// json output from the file.
			jsonExcelUtil.loadTestSource(fileSource);
			
			// Edit here ↓

			// Call this function for printing a specific json
			// output from the JSON_Data sheet.
			getJSON(JSON_DATA, "Test1");

			// Call this function for printing a specific json
			// output from the JSON_Expected sheet.
			getJSON(JSON_EXPECTED, "API_02_04_01_01_02_01-002");

			// Call this function for printing a specific json
			// output from the Datastore_Expected sheet.
			getJSON(DATASTORE_EXPECTED, "<Test Case Name Here>");

			// Call this function for printing all json
			// output from the JSON_Data sheet.
			getAllJSON(JSON_DATA);

			// Call this function for printing all json
			// output from the JSON_Expected sheet.
			getAllJSON(JSON_EXPECTED);

			// Call this function for printing all json
			// output from the Datastore_Expected sheet.
			getAllJSON(DATASTORE_EXPECTED);

		} else {
			System.err.println("*************************************");
			System.err.println("File not found.");
			System.err.println("*************************************");
			System.out.println();
		}
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Function for printing all the json output from a specified sheet.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param sheet 
	 */
	private static void getAllJSON(int sheet) {
		System.out.println("*************************************");
		JSONObject output = null;

		try {
			switch (sheet) {
			case JSON_DATA:
				System.out.println("Sheet: JSON_DATA");
				output = new JSONObject(jsonExcelUtil.getDataJSONList());
				break;
			case JSON_EXPECTED:
				System.out.println("Sheet: JSON_EXPECTED");
				output = new JSONObject(jsonExcelUtil.getExpectedJSONList());
				break;
			case DATASTORE_EXPECTED:
				System.out.println("Sheet: DATASTORE_EXPECTED");
				output = new JSONObject(jsonExcelUtil.getDatastoreJSONList());
				break;
			}

			if ("{}".equals(output.toString())) {
				System.out.println("JSON Output: {} (Empty json)");
			} else {
				System.out.println("JSON Output: " + output);
			}

		} catch (NullPointerException e) {
			System.out.println("No result found.");
		}

		System.out.println("*************************************");
		System.out.println();
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Function for printing specific json from a specified sheet.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param sheet
	 * @param caseName 
	 */
	private static void getJSON(int sheet, String caseName) {
		System.out.println("*************************************");
		JSONObject output = null;

		try {
			switch (sheet) {
			case JSON_DATA:
				System.out.println("Sheet: JSON_DATA");
				output = jsonExcelUtil.getDataJSONList().get(caseName);
				break;
			case JSON_EXPECTED:
				System.out.println("Sheet: JSON_EXPECTED");
				output = jsonExcelUtil.getExpectedJSONList().get(caseName);
				break;
			case DATASTORE_EXPECTED:
				System.out.println("Sheet: DATASTORE_EXPECTED");
				output = jsonExcelUtil.getDatastoreJSONList().get(caseName);
				break;
			}

			System.out.println("Test case: " + caseName);
			System.out.println("JSON Output: " + output);

		} catch (NullPointerException e) {
			System.out.println("No result found.");
		}

		System.out.println("*************************************");
		System.out.println();
	}

}
