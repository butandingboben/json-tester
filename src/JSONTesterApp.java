import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleConstants.ColorConstants;
import javax.swing.text.StyledDocument;

import org.json.JSONObject;

import com.rococo.loader.util.JSONExcelUtil;

import javax.swing.JTextPane;

/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class JSONTesterApp {

	private JFrame mainFrame;
	private JTextField sourceTextField;
	private JTable sheetsTable;
	private JTable testCasesTable;
	private JTextPane outputTextArea;
	private JCheckBox selectAllCheckBox;
	private JCheckBox showInvalidCheckBox;

	private File fileSource;
	private String filename;
	private JSONExcelUtil jsonExcelUtil;
	private Object[][] sheetNameList = {
			{"JSON_Data"},
			{"Expected_Output"},
			{"Datastore_Expected"},
			{"Request_Header"},
			{"Operation_Log"}
	};
	private Object[][] testCaseList = {};
	private Object[][] invalidTestCaseList = {};
	private String[] sheetsTableColumns = {"Sheet Names"};
	private String[] testCasesTableColumns = {"Validity", "Test Case"};
	private int sheetFlag = 0;
	private int caseCount = 0;
	
	DefaultTableModel sheetsTableModel = new DefaultTableModel(sheetNameList, sheetsTableColumns) {
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	DefaultTableModel testCasesTableModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JSONTesterApp window = new JSONTesterApp();
					window.mainFrame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JSONTesterApp() {
		initialize();
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
			System.err.println("Nimbus UI Look and Feel not found.");
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mainFrame = new JFrame();
		mainFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(JSONTesterApp.class.getResource("/com/sun/java/swing/plaf/windows/icons/Inform.gif")));
		mainFrame.setResizable(false);
		mainFrame.setFont(new Font("Myriad Pro", Font.PLAIN, 12));
		mainFrame.setTitle("JSON Viewer v1.0");
		mainFrame.setSize(920, 700);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		testCasesTableModel = new DefaultTableModel(testCaseList, testCasesTableColumns) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.GRAY);
		mainFrame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Menu");
		mnNewMenu.setForeground(Color.WHITE);
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setIcon(new ImageIcon("C:\\Users\\butandingboben\\Downloads\\dialog_close.png"));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmExit);
		
		JMenu mnHelo = new JMenu("Help");
		mnHelo.setForeground(Color.WHITE);
		menuBar.add(mnHelo);
		
		JMenuItem mntmHow = new JMenuItem("How to Use");
		mntmHow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String howTo = "How to Use \r\n\r\n"
						+ "1.   Browse for the excel file containing the test cases.\r\n"
						+ "2.   Select a sheet to view all its test cases.\r\n"
						+ "3.1  Select a test case to view its JSON.\r\n"
						+ "           Note: Multiple test case selection is posible.\r\n"
						+ "3.2  Check the 'Select all' checkbox to view all JSON.\r\n\r\n";
				JOptionPane.showMessageDialog(mainFrame, howTo, "How to Use", JOptionPane.PLAIN_MESSAGE);
			}
		});
		mnHelo.add(mntmHow);
		mainFrame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 334, 650);
		mainFrame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 5, 324, 75);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel testSourceHeader = new JLabel("Test Source");
		testSourceHeader.setBounds(0, 0, 324, 15);
		panel_3.add(testSourceHeader);
		testSourceHeader.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		sourceTextField = new JTextField();
		sourceTextField.setText("File name");
		sourceTextField.setBounds(70, 25, 254, 20);
		panel_3.add(sourceTextField);
		sourceTextField.setEditable(false);
		sourceTextField.setColumns(5);
		
		JLabel lblNewLabel = new JLabel("Source file:");
		lblNewLabel.setBounds(0, 25, 70, 20);
		panel_3.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JButton browseBtn = new JButton("Browse");
		browseBtn.setBounds(70, 50, 75, 25);
		panel_3.add(browseBtn);
		browseBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browseFile();
			}
		});
		browseBtn.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JButton loadBtn = new JButton("Load");
		loadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
				sheetsTable.setEnabled(true);
				testCasesTable.setEnabled(false);
				jsonExcelUtil = new JSONExcelUtil();
				jsonExcelUtil.loadTestSource(fileSource);
			}
		});
		loadBtn.setFont(new Font("Tahoma", Font.PLAIN, 12));
		loadBtn.setBounds(155, 50, 75, 25);
		panel_3.add(loadBtn);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 91, 324, 2);
		panel.add(separator);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 105, 324, 126);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblSourceSheets = new JLabel("Source Sheets");
		lblSourceSheets.setBounds(0, 0, 324, 15);
		panel_2.add(lblSourceSheets);
		lblSourceSheets.setFont(new Font("Tahoma", Font.BOLD, 12));

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 25, 324, 100);
		panel_2.add(scrollPane_1);
		sheetsTable = new JTable(sheetsTableModel);
		sheetsTable.setEnabled(false);
		sheetsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(sheetsTable);
		sheetsTable.setBorder(new LineBorder(new Color(0, 0, 0)));
		sheetsTable.getTableHeader().setReorderingAllowed(false);
		sheetsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					selectAllCheckBox.setSelected(false);
					showInvalidCheckBox.setSelected(false);
					getTestCases(sheetsTable.getSelectedRow());
				}
			}
		});
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 242, 324, 2);
		panel.add(separator_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 255, 324, 384);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblTestCases = new JLabel("Test Cases");
		lblTestCases.setBounds(0, 0, 324, 15);
		panel_1.add(lblTestCases);
		lblTestCases.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_2.setBounds(0, 50, 324, 352);
		panel_1.add(scrollPane_2);

		testCasesTable = new JTable(testCasesTableModel);
		testCasesTable.setEnabled(false);
		testCasesTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPane_2.setViewportView(testCasesTable);
		testCasesTable.setBorder(new LineBorder(new Color(0, 0, 0)));
		testCasesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					outputJSON();
				}
			}
		});
		testCasesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		testCasesTable.getColumnModel().getColumn(0).setPreferredWidth(51);
		testCasesTable.getColumnModel().getColumn(1).setPreferredWidth(255);
		testCasesTable.getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
		testCasesTable.getTableHeader().setReorderingAllowed(false);
		testCasesTable.getTableHeader().setResizingAllowed(false);
		
		selectAllCheckBox = new JCheckBox("Select all");
		selectAllCheckBox.setEnabled(false);
		selectAllCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
		        JCheckBox checkBox = (JCheckBox) arg0.getSource();
				if (checkBox.isSelected()) {
					testCasesTable.setRowSelectionInterval(
							0, testCasesTable.getModel().getRowCount()-1);
				} else {
					testCasesTable.getSelectionModel().removeSelectionInterval(
							0, testCasesTable.getModel().getRowCount()-1);
				}
			}
		});
		selectAllCheckBox.setBounds(0, 20, 80, 23);
		panel_1.add(selectAllCheckBox);
		
		showInvalidCheckBox = new JCheckBox("Show Invalid");
		showInvalidCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
		        JCheckBox checkBox = (JCheckBox) arg0.getSource();
				if (checkBox.isSelected()) {
					selectAllCheckBox.setSelected(false);
					testCasesTableModel = new DefaultTableModel(invalidTestCaseList, testCasesTableColumns) {
						public boolean isCellEditable(int row, int column) {
							return false;
						}
					};
					testCasesTable.setModel(testCasesTableModel);
					testCasesTable.setEnabled(true);
					repaintTable(testCasesTable);
				} else {
					selectAllCheckBox.setSelected(false);
					getTestCases(sheetFlag);
				}
			}
		});
		showInvalidCheckBox.setEnabled(false);
		showInvalidCheckBox.setBounds(85, 20, 97, 23);
		panel_1.add(showInvalidCheckBox);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBounds(344, 0, 560, 650);
		mainFrame.getContentPane().add(panel_4);
		
		JLabel lblJsonOutput = new JLabel("JSON Output");
		lblJsonOutput.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblJsonOutput.setBounds(10, 5, 513, 15);
		panel_4.add(lblJsonOutput);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 550, 609);
		panel_4.add(scrollPane);
		
		outputTextArea = new JTextPane();
		outputTextArea.setFont(new Font("Arial Unicode MS", Font.PLAIN, 15));
		scrollPane.setViewportView(outputTextArea);
		outputTextArea.setEditable(false);
		outputTextArea.setMargin(new Insets(10,10,10,10));
		outputTextArea.setContentType("text/html");
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01 
	 * @since 0.01
	 */
	protected void outputJSON() {
		int[] selectedRows = testCasesTable.getSelectedRows();
//		if (caseCount != testCasesTable.getSelectedRowCount()) {
//			selectAllCheckBox.setSelected(false);
//		} else {
//			selectAllCheckBox.setSelected(true);
//		}
		outputTextArea.setText("");
		for (int i = 0; i < selectedRows.length; i++) {
			int row = selectedRows[i];
			boolean validFlag = false;
			if (testCasesTable.getModel().getValueAt(row, 0).toString().equals("Valid")) {
				validFlag = true;
			}
			String testCase = testCasesTable.getModel().getValueAt(row, 1).toString();
			String output = getJSON(sheetFlag, testCase);

			printOutput(testCase, output, validFlag);
		}
		outputTextArea.setCaretPosition(0);
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testCase
	 * @param output 
	 * @since 0.01
	 */
	private void printOutput(String testCase, String output, boolean validFlag) {
	    StyledDocument document = (DefaultStyledDocument) outputTextArea.getDocument();

	    SimpleAttributeSet boldAttribute = new SimpleAttributeSet();
	    boldAttribute.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);
	    boldAttribute.addAttribute(StyleConstants.Foreground, Color.BLACK);
	    boldAttribute.addAttribute(StyleConstants.FontSize, 14);

	    SimpleAttributeSet invalidAttribute = new SimpleAttributeSet();
	    invalidAttribute.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);
	    invalidAttribute.addAttribute(StyleConstants.Foreground, Color.RED);
	    invalidAttribute.addAttribute(StyleConstants.FontSize, 14);

	    SimpleAttributeSet regularAttribute = new SimpleAttributeSet();
	    regularAttribute.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.FALSE);
	    regularAttribute.addAttribute(StyleConstants.Foreground, Color.BLACK);
	    regularAttribute.addAttribute(StyleConstants.FontSize, 13);

		try {
			document.insertString(document.getLength(), "------------------------------------------------------------"
					+ "------------------------------------------" + "\r\n", regularAttribute);
			if (validFlag) {
				document.insertString(document.getLength(), "Test Case: "
						+ testCase + "\r\n", boldAttribute);
			} else {
				document.insertString(document.getLength(), "Test Case: "
						+ testCase + "\r\n", invalidAttribute);
			}
			document.insertString(document.getLength(), "------------------------------------------------------------"
					+ "------------------------------------------" + "\r\n", regularAttribute);
			document.insertString(document.getLength(), formatOutput(output) + "\r\n\r\n", regularAttribute);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param output
	 * @return String
	 * @since 0.01
	 */
	protected String formatOutput(String output) {
		return new JSONObject(output).toString(5);
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param selectedRow 
	 * @since 0.01
	 */
	protected void getTestCases(int selectedRow) {
		sheetFlag = selectedRow;
		Object[][] tableData;
		List<String> dataList = new ArrayList<String>();
		switch (selectedRow) {
		case 0:
			dataList = jsonExcelUtil.getDataJSONCases();
			break;
		case 1:
			dataList = jsonExcelUtil.getExpectedJSONCases();
			break;
		case 2:
			dataList = jsonExcelUtil.getExpectedDatastoreCases();
			break;
		case 3:
			dataList = jsonExcelUtil.getStatusJSONCases();
			break;
		case 4:
			dataList = jsonExcelUtil.getStatusJSONCases();
			break;
		}
		caseCount = dataList.size();
		tableData = new Object[caseCount][2];
		String[] invalidTempList = new String[caseCount];
		int invalidCount = 0;
		for (int i = 0; i < caseCount; i++) {
			if (!getJSON(selectedRow, dataList.get(i)).equals("{}")) {
				tableData[i][0] = "Valid";
			} else {
				tableData[i][0] = "Invalid";
				invalidTempList[invalidCount] = dataList.get(i);
				invalidCount++;
			}
			tableData[i][1] = dataList.get(i);
		}
		invalidTestCaseList = new Object[invalidCount][2];
		for (int i = 0; i < invalidCount; i++) {
			invalidTestCaseList[i][0] = "Invalid";
			invalidTestCaseList[i][1] = invalidTempList[i];
		}
		testCasesTableModel = new DefaultTableModel(tableData, testCasesTableColumns) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		testCasesTable.setModel(testCasesTableModel);
		testCasesTable.setEnabled(true);
		repaintTable(testCasesTable);

		if (0 < caseCount) {
			selectAllCheckBox.setEnabled(true);
		} else {
			selectAllCheckBox.setEnabled(false);
		}
		if (0 < invalidTestCaseList.length) {
			showInvalidCheckBox.setEnabled(true);
		} else {
			showInvalidCheckBox.setEnabled(false);
		}
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param table 
	 *		<div class="jp">
	 *			
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			
	 *		</div>
	 * @since 0.01
	 */
	private void repaintTable(JTable table) {
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.getColumnModel().getColumn(0).setPreferredWidth(51);
		table.getColumnModel().getColumn(1).setPreferredWidth(255);
		table.getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		table.repaint();
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param selectedRow
	 * @param testCase
	 * @return 
	 * @since 0.01
	 */
	private String getJSON(int selectedRow, String testCase) {
		String json = "";
		try {
			switch (selectedRow) {
			case 0:
				json = jsonExcelUtil.getDataJSONList().get(testCase).toString();
				break;
			case 1:
				json = jsonExcelUtil.getExpectedJSONList().get(testCase).toString();
				break;
			case 2:
				json = jsonExcelUtil.getDatastoreJSONList().get(testCase).toString();
				break;
			case 3:
				json = jsonExcelUtil.getStatusJSONList().get(testCase).toString();
				break;
			case 4:
				json = jsonExcelUtil.getStatusJSONList().get(testCase).toString();
				break;
			}
		} catch (NullPointerException e) {
			json = "{}";
		}

		return json;
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01 
	 * @since 0.01
	 */
	protected void browseFile() {
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel file", "xls");
		
		if (!sourceTextField.getText().equals("File name")) {
			fileChooser = new JFileChooser(sourceTextField.getText().replace(filename, ""));
		}
		
		fileChooser.setFileFilter(filter);
		if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(mainFrame)) {
			filename = fileChooser.getSelectedFile().getName();
			if (fileChooser.getSelectedFile().getName().split("\\.")[1].equals("xls")) {
				fileSource = fileChooser.getSelectedFile();
				sourceTextField.setText(fileChooser.getSelectedFile()
						.getAbsolutePath());
				sheetsTable.setEnabled(true);
				testCasesTable.setEnabled(false);
				jsonExcelUtil = new JSONExcelUtil();
				jsonExcelUtil.loadTestSource(fileSource);
				reset();
			} else {
				JOptionPane.showMessageDialog(mainFrame, "Invalid file format.");
			}
		}
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01 
	 * @since 0.01
	 */
	private void reset() {
		// Reset components
		outputTextArea.setText("");
		selectAllCheckBox.setSelected(false);
		testCasesTableModel = new DefaultTableModel(null, testCasesTableColumns) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		testCasesTable.setModel(testCasesTableModel);
		repaintTable(testCasesTable);
		sheetsTable.getSelectionModel().removeSelectionInterval(
				0, sheetsTable.getModel().getRowCount()-1);
		testCasesTable.getSelectionModel().removeSelectionInterval(
				0, testCasesTable.getModel().getRowCount()-1);

		// Reset variables
		sheetFlag = 0;
		invalidTestCaseList = null;
	}
}
